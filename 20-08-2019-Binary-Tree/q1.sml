(*
	binary tree
	defining datatype for binary tree
*)

datatype 'a tree = nullTree
                 | Node of 'a tree*'a*'a tree;

(*
	test binary trees
	constructing
*)

(* helper function *)
fun singleNode a = Node (nullTree,a,nullTree);

val tree0 = nullTree;
val tree1 = singleNode 1;
val tree2 = singleNode 2;
val tree3 = singleNode 3;
val tree4 = Node (tree2,1,tree0);
val tree5 = Node (tree3,2,tree4);
val treeStr = Node (singleNode #"a",#"b",nullTree);
