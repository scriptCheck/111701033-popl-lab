(*
	binary tree
	map functionality for binary tree
*)

(* define a binary tree *)
datatype 'a tree = nullTree
                 | Node of 'a tree*'a*'a tree

(* in-order *)
fun inOrder nullTree       = []
  | inOrder (Node (x,z,y)) = inOrder x@[z]@inOrder y

(* pre-order *)
fun preOrder nullTree       = []
  | preOrder (Node (x,z,y)) = [z]@preOrder x@preOrder y

(* post-order *)
fun postOrder nullTree       = []
  | postOrder (Node (x,z,y)) = postOrder x@postOrder y@[z]

(* map function *)
fun map f nullTree = nullTree
  | map f (Node (x,z,y)) = Node (map f x,f z,map f y)

(*
	test binary trees
	map function
*)

(* helper function *)
fun singleNode a = Node (nullTree,a,nullTree)

val tree0 = nullTree
val tree1 = singleNode 1
val tree2 = singleNode 2
val tree3 = singleNode 3
val tree4 = Node (tree2,1,tree0)
val tree5 = Node (tree3,2,tree4)
val treeStr = Node (singleNode #"a",#"b",nullTree)

fun incr x = x+1

val treeIncr = map incr tree5
