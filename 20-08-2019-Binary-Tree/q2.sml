(*
	binary tree
	traversal for binary tree
*)

(* define a binary tree *)
datatype 'a tree = nullTree
                 | Node of 'a tree*'a*'a tree

(* in-order *)
(* inOrder : 'a tree -> 'a list *)
fun inOrder nullTree       = []
  | inOrder (Node (x,z,y)) = inOrder x@[z]@inOrder y

(* pre-order *)
(* preOrder : 'a tree -> 'a list *)
fun preOrder nullTree       = []
  | preOrder (Node (x,z,y)) = [z]@preOrder x@preOrder y

(* post-order *)
(* postOrder : 'a tree -> 'a list *)
fun postOrder nullTree       = []
  | postOrder (Node (x,z,y)) = postOrder x@postOrder y@[z]

(*
	test binary trees
	traversal
*)

(* helper function *)
fun singleNode a = Node (nullTree,a,nullTree)

val tree0 = nullTree
val tree1 = singleNode 1
val tree2 = singleNode 2
val tree3 = singleNode 3
val tree4 = Node (tree2,1,tree0)
val tree5 = Node (tree3,2,tree4)
val treeStr = Node (singleNode #"a",#"b",nullTree)

(* in order *)
val in0 = inOrder tree0
val in1 = inOrder tree1
val in2 = inOrder tree4
val in3 = inOrder tree5
val in4 = inOrder treeStr

(* pre order *)
val pre0 = preOrder tree0
val pre1 = preOrder tree1
val pre2 = preOrder tree4
val pre3 = preOrder tree5
val pre4 = preOrder treeStr

(* post order *)
val post0 = postOrder tree0
val post1 = postOrder tree1
val post2 = postOrder tree4
val post3 = postOrder tree5
val post4 = postOrder treeStr


