(*
	binary tree
	recursor function for binary tree
*)

(* define a binary tree *)
datatype 'a tree = nullTree
                 | Node of 'a tree*'a*'a tree

(* in-order *)
fun inOrder nullTree       = []
  | inOrder (Node (x,z,y)) = inOrder x@[z]@inOrder y

(* pre-order *)
fun preOrder nullTree       = []
  | preOrder (Node (x,z,y)) = [z]@preOrder x@preOrder y

(* post-order *)
fun postOrder nullTree       = []
  | postOrder (Node (x,z,y)) = postOrder x@postOrder y@[z]

(* map function *)
fun map f nullTree = nullTree
  | map f (Node (x,z,y)) = Node (map f x,f z,map f y)

(* Recursor function *)
fun recT nullTreeb Nodef nullTree = nullTreeb
  | recT nullTreeb Nodef (Node (x,z,y)) = Nodef (recT nullTreeb Nodef x,z,recT nullTreeb Nodef y)

fun rotate (Node (Node (t1,b,t2),a,t3)) = Node (t1,b,Node (t2,a,t3))
  | rotate x = x

(*
	test binary trees
	rotate function
*)

(* helper function *)
fun singleNode a = Node (nullTree,a,nullTree)

val tree0 = nullTree
val tree1 = singleNode 1
val tree2 = singleNode 2
val tree3 = singleNode 3
val tree4 = Node (tree2,1,tree0)
val treeStr = Node (singleNode #"a",#"b",nullTree)

val tree5 = Node (tree3,6,tree4)

val li5Pre = preOrder tree5
val li5In = inOrder tree5
val li5Post = postOrder tree5

val rotTree = rotate tree5

val liRotPre = preOrder rotTree
val liRotIn = inOrder rotTree
val liRotPost = postOrder rotTree


