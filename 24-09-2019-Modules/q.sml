(*
	modules: a new chapter
	modules, signatures, functors
	quicksort
*)

signature SORT = sig
	type t
	val sort : t list -> t list
end

(* datatype order = LESS | EQUAL | GREATER *)

signature ORD_KEY = sig
	type ord_key 
		(* abstract type of keys that have a total order *)
	val compare : ord_key * ord_key -> order 
		(* comparison function for the ordering of keys *)
end (* ORD_KEY *)

functor QSort ( O : ORD_KEY ) : SORT = struct
	type t = O.ord_key
	
	fun f x y = if (O.compare (x,y)) = GREATER
		    then true
		    else false
	
	fun sort [] = []
          | sort (x::xs) = let
				val pivot = x
				val partf = (f x)
				val (a,b) = List.partition partf xs
				val left = sort a
				val right = sort b
			   in
				left@[x]@right
			   end
end

(*
	testing quicksort functor
*)

structure intSimple : ORD_KEY = struct
	type ord_key = int
	val compare = fn (x,y) => if x>y
				  then GREATER
				  else
					if x=y 
					then EQUAL
					else LESS
end

structure simpleOrd = QSort (intSimple)

fun main () = (print "foo"; simpleOrd.sort [4,5,3,1,2])

(*
structure intElement : ORD_KEY = struct
	type ord_key = int
	val compare = Int.compare
end

structure IntOrd = QSort (intElement)
IntOrd.sort [4,5,3,1,2]
*)

