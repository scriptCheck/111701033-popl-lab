
(*
	structures are values
	signatures are types
*)

structure B :
	  sig
	      type t
	      val  v : t
	      val  f : t -> t
	  end
= struct

  datatype t = Foo of int | Bar of bool

  val v = Foo 42

  fun fromInt x = x > 0
  fun fromBool true  = 1
    | fromBool false = 0


  fun f (Foo x) = Bar (fromInt x)
    | f (Bar x) = Foo (fromBool x)


end

