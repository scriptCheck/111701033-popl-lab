man(arjun).
man(pandu).
man(bheem).
man(yudhistir).
man(abhimanyu).
man(vyasan).
woman(kunti).
woman(madri).
parent(arjun, pandu).
parent(arjun, kunti).
parent(bheem, pandu).
parent(bheem, kunti).
parent(yudhistir, pandu).
parent(yudhistir, kunti).
parent(nakul, pandu).
parent(nakul, madri).
parent(sahadev, pandu).
parent(sahadev, madri).
parent(abhimanyu, arjun).
parent(pandu, vyasan).

%%% Facts end here %%%%%%%%%%%%%%%%%%%

mother(X,Y)  :- woman(Y), parent(X,Y).
father(X,Y)  :- man(Y), parent(X,Y).
sibling(X,Y) :- dif(X,Y), father(X,F), father(Y,F), mother(X,M), mother(Y,M).

%% Exercise
%%
%% What happens when the sibling definition does not have dif
%%  X and Y can be the same person
%% Write a half brother relation similar to brother.
    halfbrother(X,Y) :- dif(X,Y), father(X,Z), father(Y,Z), mother(X,M1), mother(Y,M2), dif(M1,M2).
    halfbrother(X,Y) :- dif(X,Y), father(X,F1), father(Y,F2), mother(X,Z), mother(Y,Z), dif(F1,F2).

brother(X,Y) :- man(Y), sibling(X,Y).
sister(X,Y) :- woman(Y), sibling(X,Y).

ancestor(X,Y) :- parent(X,Y).
ancestor(X,Y) :- parent(X,Z),ancestor(Z,Y).

