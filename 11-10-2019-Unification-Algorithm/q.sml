(*
	unification algorithm
*)

datatype expr = V of string
              | A of (expr*expr)
              | L of (string*expr)

datatype monotype = basic of string
                  | tvar of string
                  | tarrow of (monotype*monotype)

structure Key : ORD_KEY = struct
	type ord_key = String.string
	fun compare (x,y) = String.compare (x,y)
end

structure Map = RedBlackMapFn (Key)

exception error

(* occurs: string -> monotype -> bool *)
fun occurs a (tvar t)         =  t = a 
  | occurs a (basic b)        = false
  | occurs a (tarrow (t1,t2)) = (occurs a t1) orelse (occurs a t2)

(* subst: monotype Map -> monotype -> monotype *)
fun subst telescope (tvar x)         = ( case Map.find (telescope, x) of
				  	  	  (NONE) => (tvar x)
				  		| (SOME y) => subst telescope y 
			       	       )
  | subst telescope (basic a)        = (basic a)
  | subst telescope (tarrow (t1,t2)) = let 
						val subst_t1 = subst telescope t1
						val subst_t2 = subst telescope t2
				       in
						tarrow (subst_t1,subst_t2)
				       end

fun subst_pair telescope (mono_t1, mono_t2) = (subst telescope mono_t1, subst telescope mono_t2)

fun unifyList [] = Map.empty
  | unifyList ((t1,t2)::ts) = let
				val E = unify (t1,t2)
				val ts2 = List.map (subst_pair E) ts
				val E2 = unifyList ts2
			      in
				Map.unionWith (fn (u,v) => if (u<>v) then raise error else u) (E, E2)
			      end

and unify (tvar t1, tvar t2) = if (t1=t2)
				then Map.empty
				else Map.singleton (t1, tvar t2)
  | unify (tvar t1, basic a) = Map.singleton (t1, basic a)
  | unify (tvar t1, (tarrow (t2,t3)) ) = if ( (occurs t1 t2) orelse (occurs t1 t3))
					 then raise error
					 else Map.singleton (t1, (tarrow (t2,t3)))
  | unify (basic a, tvar t1) = unify (tvar t1, basic a)
  | unify (basic a, basic b) =  if (a=b)	
				then Map.empty
			       else raise error
  | unify (basic a, tarrow (t1,t2)) = raise error
  | unify (tarrow (t1,t2), tvar t3) = unify (tvar t3,tarrow (t1,t2))
  | unify (tarrow (_,_), basic _) = raise error
  | unify (tarrow (t1,t2),tarrow (t3,t4)) = unifyList [(t1,t3),(t2,t4)]
  
(*
	testing unification algo
*)

(* test 1 *)
val a = tarrow (basic "int",basic "int")
val b = tarrow (tvar "alpha",tvar "gamma")
val x = unifyList [(a,b)];
val y = Map.listItemsi x;

(* test 2 *)
val a = tarrow (tvar "tau1",tvar "tau2")
val b = tarrow (tarrow (tvar "tau3",tvar "tau4"), tarrow (tvar "tau4",tvar "tau5") )
val x = unifyList [(a,b)]
val y = Map.listItemsi x

(* test 3 : all basics *)
val a = basic "int"
val b = basic "string"
val c = basic "int"
val x = unifyList [(a,c)]
val y = Map.listItemsi x

val z = unifyList [(a,b)]
val y = Map.listItemsi z;
