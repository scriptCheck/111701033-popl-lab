(*
	fun with lists
	defining range function
*)

(* increments numbers *)
fun incr x = x+1;

(* add numbers *)
fun plus x y = x+y;

(* map is of type ('a->'b)->'a list->'b list *)
fun map f [] = []
  | map f (x::xs) = (f x)::(map f xs);

(* 
   open List
   use fn foldl type ('a * 'b -> 'b) -> 'b -> 'a list -> 'b
*)

fun range n m = if n<=m then ((n)::(range (n+1) m)) else [];

(*
	test lists
	range operation
*)

val li = [4,3,2,1];
val li_out = range 1 4;
