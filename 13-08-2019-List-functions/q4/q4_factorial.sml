(*
	fun with lists
	defining factorial function
*)

(* increments numbers *)
fun incr x = x+1;

(* add numbers *)
fun plus x y = x+y;

(* map is of type ('a->'b)->'a list->'b list *)
fun map f [] = []
  | map f (x::xs) = (f x)::(map f xs);

(* 
   open List
   use fn foldl type ('a * 'b -> 'b) -> 'b -> 'a list -> 'b
*)

open List;

fun range n m = if n<=m then ((n)::(range (n+1) m)) else [];

fun mul (x,y) = x*y

(* factorial(n) = n*(factorial(n-1) *)
fun factorial 0 = 1
  | factorial n = foldl (mul) 1 (range 1 n);

(*
	test lists
	range operation
*)

val x = 5;
val x_ = factorial x;
