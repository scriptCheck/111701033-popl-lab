(*
	fun with lists
	simple isEmpty function
*)

fun isEmpty [] = true
  | isEmpty _ = false;

(*
	test on lists
	check isEmpty function
*)

val a = [];
val b = [[]];
val c = [3];
val d = ["string"];
;
val case1 = isEmpty a;
;
val case2 = isEmpty b;
;
val case3 = isEmpty c;
;
val case4 = isEmpty d;
;

