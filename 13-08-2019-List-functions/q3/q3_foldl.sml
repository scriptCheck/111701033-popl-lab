(*
	fun with lists
	define foldl function
*)

(* increments numbers *)
fun incr x = x+1;

(* add numbers *)
fun plus x y = x+y;

(* map is of type ('a->'b)->'a list->'b list *)
fun map f [] = []
  | map f (x::xs) = (f x)::(map f xs);

(* val foldl : ('b -> 'a -> 'b) -> 'b      -> 'a list -> 'b
   If you thing of `f` as an operator ⊙ then the folds can be seen as
   foldl ⊙ b₀ [a₀,....,aₙ]     = (((b₀ ⊙ a₀) ⊙ a₁) ... ⊙ aₙ₋₁) ⊙ aₙ
*)
fun foldl f b ([]) = b
  | foldl f b (x::xs) = f (foldl f b xs) x;

(*
	test lists
	foldl operation
*)

val li = [4,3,2,1];
val li_out = foldl plus 0 li;
