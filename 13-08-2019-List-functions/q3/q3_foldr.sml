(*
	fun with lists
	defining foldr function
*)

(* increments numbers *)
fun incr x = x+1;

(* add numbers *)
fun plus x y = x+y;

(* map is of type ('a->'b)->'a list->'b list *)
fun map f [] = []
  | map f (x::xs) = (f x)::(map f xs);

(* val foldr : ('a -> 'b -> 'b) -> 'a list -> 'b      -> 'b
   If you thing of `f` as an operator ⊙ then the folds can be seen as
   foldr ⊙    [a₀,....,aₙ]  b₀ = a₀ ⊙ (a₁ ⊙ (.... (aₙ ⊙ b₀)))
*)
fun foldr f ([]) b = b
  | foldr f (x::xs) b = f x (foldr f xs b)

(*
	test lists
	foldr operation
*)

val li = [1,2,3,4];
val li_out = foldr plus li 0;

val li2 = [];
val li2_out = foldr plus li 2;
