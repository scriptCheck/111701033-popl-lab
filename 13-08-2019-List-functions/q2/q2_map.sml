(*
	fun with lists
	defining map function
*)

(* increments numbers *)
fun incr x = x+1;

(* map is of type ('a->'b)->'a list->'b list *)
fun map f [] = []
  | map f (x::xs) = (f x)::(map f xs);

(*
	test lists
	map operation
*)

val li = [1,2,3,4];
val li_out = map incr li;
