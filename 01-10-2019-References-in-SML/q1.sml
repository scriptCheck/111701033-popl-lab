(*
	references
	counter
*)

signature COUNTER = sig
	val incr : unit -> unit
	val set : int -> unit
	val get : unit -> int
end

structure Counter : COUNTER = struct

val c = ref 0

(*
val _ = c := 42
val vx = !x
*)

fun set y = c:=y
fun get () = !c
fun incr () = c:=get () +1

end

(*
	testing counter
*)

val i = Counter.set 42
val v = Counter.get ()
val p = Counter.incr ()
val newv = Counter.get ()
		 
