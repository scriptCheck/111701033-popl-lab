(*
	references
	counter
*)

signature COUNTER = sig
	val incr : unit -> unit
	val set : int -> unit
	val get : unit -> int
end

structure Counter : COUNTER = struct

val c = ref 0

(*
val _ = c := 42
val vx = !x
*)

fun set y = c:=y
fun get () = !c
fun incr () = c:=get () +1

end

functor MkCounter () : COUNTER = struct
val c = ref 0

fun set y = c:=y
fun get () = !c
fun incr () = c:=get () +1

end

(*
	testing MkCounter
*)

structure A = MkCounter ()
structure B = MkCounter ()

val ai = A.set 42
val bi = B.set 56
val ag = A.get ()
val bg = B.get ()
val ac = A.incr ()
val bc = B.incr ()
val agnew = A.get ()
val bgnew = B.get ()

