(*
	lambda calculus : the inception
	expr
	free, bound
	fresh
	subst
*)

open String
open List

(* *)
datatype expr 	= VAR of string
		| APP of expr*expr
		| ABS of string*expr

(*
fun _ (VAR str) = 
  | _ (APP (e1,e2)) = 
  | _ (ABS (str,e)) = 
*)

(* *)
fun free (VAR str) = [str]
  | free (APP (e1,e2)) = (free e1)@(free e2)
  | free (ABS (str,e)) = let
				fun fstr x = if (String.compare (str,x)) = EQUAL then
						false
					     else
						true
			 in
				List.filter fstr (free e)
			 end

fun bound (VAR str) = []
  | bound (APP (e1,e2)) = (bound e1)@(bound e2)
  | bound (ABS (str,e)) = str::(bound e)

(* *)
fun change #"a" = #"b"
  | change  _  = #"a"

fun diag (st,x) = if (isPrefix x st) then
 			implode ((explode x)@[change (hd (explode (String.extract (st, size x,NONE))))])
 		  else	
 			x;

fun fresh li= foldl diag "z" li

(* *)
fun subst (VAR str) x e = if (String.compare (str,x))=EQUAL then 
				e
			  else
				(VAR str)
  | subst (APP (e1,e2)) x e = (APP (subst e1 x e,subst e2 x e))
  | subst (ABS (str,a)) x e = if (String.compare (str,x))=EQUAL then
				(ABS (str,a))
			      else
				(ABS (str,(subst a x e)))

(*
	testing lambda calculus
*)


