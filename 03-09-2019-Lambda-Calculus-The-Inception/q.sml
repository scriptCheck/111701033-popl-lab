(*
	lambda calculus : the inception
	expr
	free, bound
	fresh
	subst
*)

open String
open List

(* *)
datatype expr 	= VAR of string
		| APP of expr*expr
		| ABS of string*expr

(*
fun _ (VAR str) = 
  | _ (APP (e1,e2)) = 
  | _ (ABS (str,e)) = 
*)

(* *)
fun free (VAR str) = [str]
  | free (APP (e1,e2)) = (free e1)@(free e2)
  | free (ABS (str,e)) = let
				fun fstr x = if (String.compare (str,x)) = EQUAL then
						false
					     else
						true
			 in
				List.filter fstr (free e)
			 end

fun bound (VAR str) = []
  | bound (APP (e1,e2)) = (bound e1)@(bound e2)
  | bound (ABS (str,e)) = str::(bound e)

(* *)
fun diag "" "" = "a"
  | diag "" str = let 
			val l = String.explode str
		  in
			if (String.compare (hd l,"a")) = EQUAL then
				String.implode ("b"::(tl l))
			else
				str
		  end
  | diag str "" = str
  | diag str1 str2 = let
			val l1 = String.explode str1
			val l2 = String.explode str2 
		     in
			if (String.compare (hd l1,hd l2))=EQUAL then
				implode (hd l1)::(explode (diag (tl l1) (tl l2)))
			else
				str1
		     end

fun fresh stringList = foldl diag "a" stringList

(* *)
fun subst (VAR str) x e = if (str,x)=EQUAL then 
				e
			  else
				(VAR str)
  | subst (APP (e1,e2)) x e = (APP (subst e1 x e) (subst e2 x e))
  | subst (ABS (str,a)) x e = if (str,x)=EQUAL then
				(ABS y a)
			      else
				(ABS y (subst a x e))

(*
	testing lambda calculus
*)

val li = [1,2,3]
val rli = rev li
val rli_r = rev_r li

val ni = ["hello","world"]
val rni = rev ni
val rni_r = rev_r ni

