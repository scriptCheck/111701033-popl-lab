(*
	lambda calculus : the inception
	expr
	free, bound
	fresh
	subst
*)

open String
open List

(* *)
datatype expr 	= VAR of string
		| APP of expr*expr
		| ABS of string*expr

(*
fun _ (VAR str) = 
  | _ (APP (e1,e2)) = 
  | _ (ABS (str,e)) = 
*)

(* *)
fun free (VAR str) = [str]
  | free (APP (e1,e2)) = (free e1)@(free e2)
  | free (ABS (str,e)) = let
				fun fstr x = if (String.compare (str,x)) = EQUAL then
						false
					     else
						true
			 in
				List.filter fstr (free e)
			 end

fun bound (VAR str) = []
  | bound (APP (e1,e2)) = (bound e1)@(bound e2)
  | bound (ABS (str,e)) = str::(bound e)

(*
	testing lambda calculus
*)


