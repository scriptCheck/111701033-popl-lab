(*
	lambda calculus : the inception
	expr
	free, bound
	fresh
	subst
*)

open String
open List

(* *)
datatype expr 	= VAR of string
		| APP of expr*expr
		| ABS of string*expr


(*
	testing lambda calculus
*)


