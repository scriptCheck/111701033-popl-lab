(*
	lambda calculus : the inception
	expr
	free, bound
	fresh
	subst
*)

open String
open List

(* *)
datatype expr 	= VAR of string
		| APP of expr*expr
		| ABS of string*expr

(*
fun _ (VAR str) = 
  | _ (APP (e1,e2)) = 
  | _ (ABS (str,e)) = 
*)

(* *)
fun free (VAR str) = [str]
  | free (APP (e1,e2)) = (free e1)@(free e2)
  | free (ABS (str,e)) = let
				fun fstr x = if (String.compare (str,x)) = EQUAL then
						false
					     else
						true
			 in
				List.filter fstr (free e)
			 end

fun bound (VAR str) = []
  | bound (APP (e1,e2)) = (bound e1)@(bound e2)
  | bound (ABS (str,e)) = str::(bound e)

(* *)
fun change #"a" = #"b"
  | change  _  = #"a"

fun diag (st,x) = if (isPrefix x st) then
 			let
				val l_extract = String.extract (st,size x,NONE)
				val l_charList = explode l_extract
				val l_first = hd l_charList
				val l_new = change l_first
				val x_charList = explode x
			in
				implode (x_charList@[l_new])
			end
 		  else	
 			x;

fun fresh li= foldl diag "z" li

(*
	testing lambda calculus
*)

fresh ["a","ba","bba","i","j","K","l","bbba"]

