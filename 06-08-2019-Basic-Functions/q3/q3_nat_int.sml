(*
	new datatype for integers: Nat
	conversion between Nat and Int
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

(*
	test new datatype
*)
val x = (S (S (S (S (S O) ) ) ) );
val x_int = toInt x;
