(*
	new datatype for integers: Nat
	defining exponentiation
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(* helper function *)
fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

(* addition operation *)
fun plus O y = y
  | plus (S x) y = S (plus x y)

(* multiplication operation *)
fun mul O y = O
  | mul (S x) y = plus (mul x y) y

fun pow O O = (S O)
  | pow O _ = O
  | pow _ O = (S O)
  | pow y (S x) = mul y (pow y x)

(*
	test new operation - exponentiation
*)
val a = (S (S (S (S (S O) ) ) ) ); (* 5 *)
val b = O; (* 0 *)
val c = (S (S (S O) ) ); (* 3 *)
val d = (S O); (* 1 *)
;
val a_int = toInt a;
val b_int = toInt b;
val c_int = toInt c;
val d_int = toInt d;
;
val case1 = pow O b;
val case1_int = toInt case1;
;
val case2 = pow O c;
val case2_int = toInt case2;
;
val case3 = pow a b;
val case3_int = toInt case3;
;
val case4 = pow d c;
val case4_int = toInt case4;
;
val case5 = pow c d;
val case5_int = toInt case5;
;
val case6 = pow a c;
val case6_int = toInt case6;
;
