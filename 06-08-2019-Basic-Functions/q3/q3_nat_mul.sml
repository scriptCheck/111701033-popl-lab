(*
	new datatype for integers: Nat
	defining multiplication operation
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(* helper function *)
fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

fun plus O y = y
  | plus (S x) y = S (plus x y)

fun mul O y = O
  | mul (S x) y = plus (mul x y) y

(*
	test new operation - multiplication
*)
val c = (S (S (S (S (S O) ) ) ) ); (* 5 *)
val b = O; (* 0 *)
val a = (S (S (S O) ) ); (* 3 *)

val a_int = toInt a;
val b_int = toInt b;
val c_int = toInt c;

val case1 = mul O b;
val case1_int = toInt case1;

val case2 = mul O a;
val case2_int = toInt case2;

val case3 = mul c b;
val case3_int = toInt case3;

val case4 = mul c a;
val case4_int = toInt case4;

