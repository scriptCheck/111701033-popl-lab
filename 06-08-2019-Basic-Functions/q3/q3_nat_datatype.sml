(*
	new datatype for integers
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(*
	test new datatype
*)
val x = O;
