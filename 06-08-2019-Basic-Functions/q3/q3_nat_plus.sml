(*
	new datatype for integers: Nat
	conversion between Nat and Int
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(* helper function *)
fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

fun plus O y = y
  | plus (S x) y = S (plus x y)

(*
	test new operation - plus
*)
val a = (S (S (S (S (S O) ) ) ) );
val b = O;
val c = (S (S (S O) ) );

val a_int = toInt a;
val b_int = toInt b;
val c_int = toInt c;

val case1 = plus O b;
val case1_int = toInt case1;

val case2 = plus O c;
val case2_int = toInt case2;

val case3 = plus a b;
val case3_int = toInt case3;

val case4 = plus a c;
val case4_int = toInt case4;

