(*
	pass a tuple of size 3 and return the third element
*)

fun thd (_,_,z) = z;

(*
	test
*)

val a = (1,"scriptCheck",90.0);
val b = thd a;
