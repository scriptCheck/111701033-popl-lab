(*
	pass a tuple of size 3 and return the first element
*)

fun fst (x,_,_) = x;

(*
	test
*)

val a = (1,"scriptCheck",90.0);
val b = fst a;
