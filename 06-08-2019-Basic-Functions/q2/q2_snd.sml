(*
	pass a tuple of size 3 and return the second element
*)

fun snd (_,y,_) = y;

(*
	test
*)

val a = (1,"scriptCheck",90.0);
val b = snd a;
