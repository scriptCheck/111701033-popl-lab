(*
	new datatype for integers: Nat
	Part 2
	defined an iterator
	define multiplication
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(* helper function *)
fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

(* addition operation *)
fun plus O y = y
  | plus (S x) y = S (plus x y);

(* multiplication operation *)
fun mul O y = O
  | mul (S x) y = plus (mul x y) y;

(* exponentiation operation *)
fun pow O O = (S O)
  | pow O _ = O
  | pow _ O = (S O)
  | pow y (S x) = mul y (pow y x);

(* iterator function *)
fun iterate O f x0 = x0
  | iterate (S n) f x0 = f (iterate n f x0);

(* addition of x and y is the x-th successor from y *)
fun plus_iter O y = y
  | plus_iter x y = iterate x S y;

fun mul_iter O y = O
  | mul_iter (S x) y = iterate x (plus_iter y) y;

(*
	test operations iterative method
	plus operation
*)

val a = (S (S (S (S (S O) ) ) ) ); (* 5 *)
val b = O; (* 0 *)
val c = (S (S (S O) ) ); (* 3 *)
val d = (S O); (* 1 *)
;
val a_int = toInt a;
val b_int = toInt b;
val c_int = toInt c;
val d_int = toInt d;
;
val case1 = toInt (mul O b);
val case1_iter = toInt (mul_iter O b);
;
val case2 = toInt (mul O c);
val case2_iter = toInt (mul_iter O c);
;
val case3 = toInt (mul a b);
val case3_iter = toInt (mul_iter a b);
;
val case4 = toInt (mul d c);
val case4_iter = toInt (mul_iter d c);
;
val case5 = toInt (mul c d);
val case5_iter = toInt (mul_iter c d);
;
val case6 = toInt (mul a c);
val case6_iter = toInt (mul_iter a c);
;
