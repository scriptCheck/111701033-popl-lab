(*
	new datatype for integers: Nat
	Part 2
	defined an iterator
	define exponentiation
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(* helper function *)
fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

(* addition operation *)
fun plus O y = y
  | plus (S x) y = S (plus x y);

(* multiplication operation *)
fun mul O y = O
  | mul (S x) y = plus (mul x y) y;

(* exponentiation operation *)
fun pow O O = (S O)
  | pow O _ = O
  | pow _ O = (S O)
  | pow y (S x) = mul y (pow y x);

(* iterator function *)
fun iterate O f x0 = x0
  | iterate (S n) f x0 = f (iterate n f x0);

(* iterative addition *)
(* addition of x and y is the x-th successor from y *)
fun plus_iter O y = y
  | plus_iter x y = iterate x S y;

(* iterative multiplication *)
(* multiplication x y is plus of y with y x-times *)
fun mul_iter O y = O
  | mul_iter (S x) y = iterate x (plus_iter y) y;

(* exponentiation x y is mul of x with x y-times *)
(* Note: pow x y != pow y x *)
fun pow_iter O O = (S O)
  | pow_iter O y = O
  | pow_iter y O = (S O)
  | pow_iter y (S x) = iterate x (mul_iter y) y;

(*
	test operations iterative method
	plus operation
*)

val a = (S (S (S (S (S O) ) ) ) ); (* 5 *)
val b = O; (* 0 *)
val c = (S (S (S O) ) ); (* 3 *)
val d = (S O); (* 1 *)
;
val a_int = toInt a;
val b_int = toInt b;
val c_int = toInt c;
val d_int = toInt d;
;
val case1 = toInt (pow O b);
val case1_iter = toInt (pow_iter O b);
;
val case2 = toInt (pow O c);
val case2_iter = toInt (pow_iter O c);
;
val case3 = toInt (pow a b);
val case3_iter = toInt (pow_iter a b);
;
val case4 = toInt (pow d c);
val case4_iter = toInt (pow_iter d c);
;
val case5 = toInt (pow c d);
val case5_iter = toInt (pow_iter c d);
;
val case6 = toInt (pow a c);
val case6_iter = toInt (pow_iter a c);
;
