(*
	new datatype for integers: Nat
	Part 2
	defining an iterator
*)

datatype Nat = O | S of Nat;
(* 'capital O' and 'capital S' (Nat->Nat) *)

(* helper function *)
fun toInt O = 0
  | toInt (S x) = (toInt x)+1;

(* addition operation *)
fun plus O y = y
  | plus (S x) y = S (plus x y);

(* multiplication operation *)
fun mul O y = O
  | mul (S x) y = plus (mul x y) y;

(* exponentiation operation *)
fun pow O O = (S O)
  | pow O _ = O
  | pow _ O = (S O)
  | pow y (S x) = mul y (pow y x);

fun iterate O f x0 = x0
  | iterate (S n) f x0 = f (iterate n f x0);

(*
	test operations iterative method
*)

val a = (S (S (S (S (S O) ) ) ) ); (* 5 *)
val b = O; (* 0 *)
val c = (S (S (S O) ) ); (* 3 *)
val d = (S O); (* 1 *)
;
val a_int = toInt a;
val b_int = toInt b;
val c_int = toInt c;
val d_int = toInt d;
;
