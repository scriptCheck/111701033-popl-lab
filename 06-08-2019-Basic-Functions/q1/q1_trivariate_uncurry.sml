fun uncurry f (x,y,z) = f x y z;

(*
	test run
*)

fun addp_c x y z  = x+y+z;
fun addp_unc (x,y,z) = x+y+z;

val unaddp_mod = uncurry addp_c;
