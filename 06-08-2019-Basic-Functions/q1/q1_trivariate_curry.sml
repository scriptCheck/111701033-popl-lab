fun curry f x y z = f(x,y,z);

(*
	test run
*)

fun addp_unc (x,y,z) = x+y+z;
fun addp_c x y z  = x+y+z;

val addp_mod = curry addp_unc;
