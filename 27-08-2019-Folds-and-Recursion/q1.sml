(*
	recursor function of lists
	recList
	nth: 'a list * n -> 'a option
*)

(* Recursor function *)
(* recList: 'b->('a->'b)->'a list->'b *)
fun recList nilb consf [] = nilb
  | recList nilb consf (x::xs) = consf (x,recList nilb consf xs)

(* helper datatype *)
datatype 'a option = NONE
		   | SOME of 'a

(* function definition using pattern matching *)
(* nth: 'a list->int->'a option *)
fun nth [] n = NONE
  | nth (x::xs) n = if (n=0) then
			(SOME x)
		    else
			nth xs (n-1)
			    
(* function definition using recList *)
(* 'a in recList definition remains the same *)
(* 'b in recList definition is (int->'a option) *)
fun nilb_nth x = NONE
fun consf_nth (a,f) n = if (n=0) then
			SOME a
		    else
			f (n-1)
fun nth_r li n = recList nilb_nth consf_nth li n

(*
	test recList
	nth function
*)

val li = [1,2,3]
val first = nth li 1

val first_r = nth_r li 1


