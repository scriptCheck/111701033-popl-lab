(*
	recursor function of lists
	recList
	reverse: 'a list->'a list
*)

(* Recursor function *)
(* recList: 'b->('a*'b->'b)->'a list->'b *)
fun recList nilb consf [] = nilb
  | recList nilb consf (x::xs) = consf (x,recList nilb consf xs)

(* function definition using pattern matching *)
(* reverse: 'a list->'a list *)
fun rev [] = []
  | rev (x::xs) = (rev xs)@[x]
			    
(* function definition using recList *)
(* 'a in recList definition remains as 'a *)
(* 'b in recList definition becomes 'a list *)
val nilb_rev  = []
fun consf_rev (a,rest) = rest@[a]
fun rev_r li = recList nilb_rev consf_rev li

(*
	test recList
	reverse function
*)

val li = [1,2,3]
val rli = rev li
val rli_r = rev_r li

val ni = ["hello","world"]
val rni = rev ni
val rni_r = rev_r ni

