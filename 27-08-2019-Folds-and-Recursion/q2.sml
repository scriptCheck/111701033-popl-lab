(*
	recursor function of lists
	recList
	partition: 'a list->('a->bool)->('a list*'a list)
*)

(* Recursor function *)
(* recList: 'b->('a*'b->'b)->'a list->'b *)
fun recList nilb consf [] = nilb
  | recList nilb consf (x::xs) = consf (x,recList nilb consf xs)

(* helper function *)
fun f_help (x,y) (z,w) = (x@z,y@w)

(* function definition using pattern matching *)
(* partition:('a->bool)-> 'a list->('a list*'a list) *)
fun partition f [] = ([],[])
  | partition f (x::xs) = if (f x=true) then
                             (f_help ([x],[]) (partition f xs))
                          else
                             (f_help ([],[x]) (partition f xs))

(* function definition using recList *)
(* 'a defined in recList remains same *)
(* 'b defined in recList becomes of type (('a->bool)->('a list*'a list)) *)
fun nilb_p _ = ([],[])
fun consf_p (a,frec) fbool = if (fbool a=true) then
                                (f_help ([a],[]) (frec fbool))
                             else
                                (f_help ([],[a]) (frec fbool))
fun partition_r f li = recList nilb_p consf_p li f

(*
	test recList
	partition function
*)

fun even x = if (x mod 2=0) then 
                true
             else
                false
val li = [1,2,3,4,5];
val first = partition even li
val first_r = partition_r even li


