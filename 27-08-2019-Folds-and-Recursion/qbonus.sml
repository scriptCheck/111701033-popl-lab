(*
	bonus question
	foldr to foldl and vice versa
*)

(* increments numbers *)
fun incr x = x+1

(* add numbers *)
fun plus x y = x+y

(* foldr : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b *)
fun foldr f ([]) b = b
  | foldr f (x::xs) b = f x (foldr f xs b)

(* foldl :  ('b -> 'a -> 'b) -> 'b -> 'a list -> 'b *)
fun foldl f b [] = b
  | foldl f b (x::xs) = f (foldl f b xs) x

(* helper function *)
(* conv: ('b->'a->'b)->('a->'b->'b) *)
fun conv f a b = f b a

(* covRL: (('a -> 'b -> 'b) -> 'a list -> 'b -> 'b)
        ->(('b -> 'a -> 'b) -> 'b -> 'a list -> 'b) *)
fun covRL fr f b [] = fr (conv f) [] b
  | covRL fr f b (x::xs) = fr (conv f) (x::xs) b

(*
	test lists
	foldr operation
*)

val li = [1,2,3,4]
val li_fr = foldr plus li 0
val li_fl = foldl plus 0 li
val li_frl = covRL foldr plus 0 li

val li2 = [];
val li2_out = foldr plus li 2;
