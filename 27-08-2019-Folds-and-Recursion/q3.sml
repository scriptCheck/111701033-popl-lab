(*
	recursor function of lists
	recList
	map: ('a->'b)->'a list->'b list
*)

(* Recursor function *)
(* recList: 'b->('a*'b->'b)->'a list->'b *)
fun recList nilb consf [] = nilb
  | recList nilb consf (x::xs) = consf (x,recList nilb consf xs)

(* function definition using pattern matching *)
(* map: ('a->'b)->'a list->'b list *)
fun map f [] = [] 
  | map f (x::xs) = (f x)::(map f xs)
			    
(* function definition using recList *)
(* 'a defined in recList remains same *)
(* 'b defined in recList becomes of type (('a->'b)->'b list) *)
fun nilb _ = []
fun consf (a,f0) f1 = (f1 a)::(f0 f1)
fun map_r f li = recList nilb consf li f

(*
	test recList
	map function
*)

fun incr x = x+1;
val li = [1,2,3,4,5];
val first = map incr li

val first_r = map_r incr li


